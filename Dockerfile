FROM openjdk:11
ADD target/user-service-app.jar user-service-app.jar
EXPOSE 8080
ENTRYPOINT ["java" , "-jar" , "user-service-app"]
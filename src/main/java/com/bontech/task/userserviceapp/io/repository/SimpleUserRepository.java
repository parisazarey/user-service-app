package com.bontech.task.userserviceapp.io.repository;

import com.bontech.task.userserviceapp.io.entity.SimpleUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SimpleUserRepository extends JpaRepository<SimpleUserEntity ,Long> {
    SimpleUserEntity findByUsername(String username);

    SimpleUserEntity findByUserId(String userId);
}

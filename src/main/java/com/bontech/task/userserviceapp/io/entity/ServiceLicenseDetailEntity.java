package com.bontech.task.userserviceapp.io.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "service_license_details")
public class ServiceLicenseDetailEntity implements Serializable {


    private static final long serialVersionUID = 5691538714889923404L;
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "service_license_id")
    private ServiceLicenseEntity service;

    @Column(nullable = false)//columnDefinition = "true for allowed"
    private boolean allowed;

    @ManyToOne
    @JoinColumn(name = "admin_users_id")
    private AdminUserEntity operator;

    @Column(nullable = false)
    private Date createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ServiceLicenseEntity getService() {
        return service;
    }

    public void setService(ServiceLicenseEntity service) {
        this.service = service;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    public AdminUserEntity getOperator() {
        return operator;
    }

    public void setOperator(AdminUserEntity operator) {
        this.operator = operator;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

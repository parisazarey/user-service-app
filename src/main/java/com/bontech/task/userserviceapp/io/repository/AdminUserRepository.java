package com.bontech.task.userserviceapp.io.repository;

import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminUserRepository extends JpaRepository<AdminUserEntity , Long> {
    AdminUserEntity findByUsername(String username);

    AdminUserEntity findByUserId(String userId);
}

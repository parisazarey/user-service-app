package com.bontech.task.userserviceapp.io.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "active_services")
public class ActiveServiceEntity implements Serializable {

    private static final long serialVersionUID = -2424243320304187343L;
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private  String activeServiceId;


    @ManyToOne
    @JoinColumn(name = "services_id")
    private ServiceEntity service;

    @Column(nullable = false)
    private Date starting;

    @Column(nullable = false)
    private Date ending;

    @Column( nullable = false)//true , for active/deactive by admin
    private boolean activationByAdmin;

    @ManyToOne
    @JoinColumn(name = "admin_users_id")
    private AdminUserEntity createdBy;

    @Column(nullable = false)
    private Date createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ServiceEntity getService() {
        return service;
    }

    public void setService(ServiceEntity service) {
        this.service = service;
    }

    public Date getStarting() {
        return starting;
    }

    public void setStarting(Date starting) {
        this.starting = starting;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public boolean isActivationByAdmin() {
        return activationByAdmin;
    }

    public void setActivationByAdmin(boolean activationByAdmin) {
        this.activationByAdmin = activationByAdmin;
    }

    @Transient
    public boolean isActive() {
        Date now = new Date();
        if (now.before(this.ending) & now.after(this.starting)) {
            return this.isActivationByAdmin();
        }
        return false;
    }

    public AdminUserEntity getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserEntity createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getActiveServiceId() {
        return activeServiceId;
    }

    public void setActiveServiceId(String activeServiceId) {
        this.activeServiceId = activeServiceId;
    }
}

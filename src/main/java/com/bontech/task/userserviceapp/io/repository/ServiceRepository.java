package com.bontech.task.userserviceapp.io.repository;

import com.bontech.task.userserviceapp.io.entity.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceEntity , Long> {
    ServiceEntity findByName(String name);

    ServiceEntity findByServiceId(String serviceId);
}

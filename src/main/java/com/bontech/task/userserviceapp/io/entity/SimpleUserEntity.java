package com.bontech.task.userserviceapp.io.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "simple_users")
public class SimpleUserEntity  implements Serializable {

    private static final long serialVersionUID = 7020932150163234569L;
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String userId;

    @Column(nullable = false , length = 80)
    private  String username;

    @Column(nullable = false)
    private  String encryptedPassword;

    @Column(nullable = false)
    private double amount;

    @ManyToOne
    @JoinColumn(name = "admin_users_id")
    private  AdminUserEntity createdBy;

    @Column(nullable = false)
    private Date createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public AdminUserEntity getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserEntity createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

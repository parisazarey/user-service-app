package com.bontech.task.userserviceapp.io.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "transactions")
public class TransactionEntity implements Serializable {

    private static final long serialVersionUID = 3637305792150410003L;
    @Id
    @GeneratedValue
    private long id;
    @OneToOne
    @JoinColumn(name = "active_services_id")
    private ActiveServiceEntity activeService;


    @OneToOne
    @JoinColumn(name = "simple_users_id")
    private SimpleUserEntity user;


    @Column(nullable = false)
    private Date date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActiveServiceEntity getActiveService() {
        return activeService;
    }

    public void setActiveService(ActiveServiceEntity activeService) {
        this.activeService = activeService;
    }

    public SimpleUserEntity getUser() {
        return user;
    }

    public void setUser(SimpleUserEntity user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

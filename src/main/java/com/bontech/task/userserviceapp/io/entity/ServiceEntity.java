package com.bontech.task.userserviceapp.io.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity(name = "services")
public class ServiceEntity implements Serializable {
    private static final long serialVersionUID = 965845036602206331L;

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String serviceId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private int maximumTimes;

    @ManyToOne
    @JoinColumn(name = "admin_users_id")
    private  AdminUserEntity createdBy;

    @Column(nullable = false)
    private Date createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMaximumTimes() {
        return maximumTimes;
    }

    public void setMaximumTimes(int maximumTimes) {
        this.maximumTimes = maximumTimes;
    }

    public AdminUserEntity getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserEntity createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

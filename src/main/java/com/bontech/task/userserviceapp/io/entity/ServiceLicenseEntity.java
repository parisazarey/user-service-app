package com.bontech.task.userserviceapp.io.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity(name = "service_licenses")
public class ServiceLicenseEntity implements Serializable {
    private static final long serialVersionUID = 7780900166440616592L;

    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    @JoinColumn(name = "service_id")
    private ServiceEntity service;

    @OneToOne
    @JoinColumn(name = "simple_users_id")
    private SimpleUserEntity simpleUserEntity;

    @ManyToOne
    @JoinColumn(name = "admin_users_id")
    private  AdminUserEntity createdBy;

    @Column(nullable = false)
    private Date createdDate;

    @Column(nullable = false)//columnDefinition = "true for allowed"
    private boolean allowed;

    @OneToMany(mappedBy = "service" , cascade = CascadeType.ALL)
    private List<ServiceLicenseDetailEntity> history;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ServiceEntity getService() {
        return service;
    }

    public void setService(ServiceEntity service) {
        this.service = service;
    }

    public SimpleUserEntity getSimpleUserEntity() {
        return simpleUserEntity;
    }

    public void setSimpleUserEntity(SimpleUserEntity simpleUserEntity) {
        this.simpleUserEntity = simpleUserEntity;
    }

    public AdminUserEntity getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserEntity createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    public List<ServiceLicenseDetailEntity> getHistory() {
        return history;
    }

    public void setHistory(List<ServiceLicenseDetailEntity> history) {
        this.history = history;
    }
}

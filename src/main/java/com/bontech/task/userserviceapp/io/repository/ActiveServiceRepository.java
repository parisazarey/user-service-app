package com.bontech.task.userserviceapp.io.repository;

import com.bontech.task.userserviceapp.io.entity.ActiveServiceEntity;
import com.bontech.task.userserviceapp.io.entity.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface ActiveServiceRepository  extends JpaRepository<ActiveServiceEntity ,Long> {
    List<ActiveServiceEntity>  findAllByServiceAndStartingBetween(ServiceEntity service , Date starting_dateStart , Date starting_date_end);
}

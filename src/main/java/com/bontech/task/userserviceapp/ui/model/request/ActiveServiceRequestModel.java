package com.bontech.task.userserviceapp.ui.model.request;

import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;
import com.bontech.task.userserviceapp.shared.dto.ServiceDTO;

import java.util.Date;

public class ActiveServiceRequestModel {
    private Date starting;
    private Date ending;
    private boolean activationByAdmin = true;

    public Date getStarting() {
        return starting;
    }

    public void setStarting(Date starting) {
        this.starting = starting;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public boolean isActivationByAdmin() {
        return activationByAdmin;
    }

    public void setActivationByAdmin(boolean activationByAdmin) {
        this.activationByAdmin = activationByAdmin;
    }
}

package com.bontech.task.userserviceapp.ui.model.response;

import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;

import java.util.Date;

public class SimpleUserRest {

    private String userId;
    private String username;
    private double amount;
    private AdminUserRest createdBy;
    private Date createdDate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public AdminUserRest getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserRest createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

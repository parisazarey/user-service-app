package com.bontech.task.userserviceapp.ui.model.response;

import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;
import com.bontech.task.userserviceapp.shared.dto.ServiceDTO;

import java.util.Date;

public class ActiveServiceRest {
    private  String activeServiceId;
    private ServiceRest service;
    private Date starting;
    private Date ending;
    private boolean activationByAdmin ;
    private AdminUserRest createdBy;
    private Date createdDate;

    public String getActiveServiceId() {
        return activeServiceId;
    }

    public void setActiveServiceId(String activeServiceId) {
        this.activeServiceId = activeServiceId;
    }

    public ServiceRest getService() {
        return service;
    }

    public void setService(ServiceRest service) {
        this.service = service;
    }

    public Date getStarting() {
        return starting;
    }

    public void setStarting(Date starting) {
        this.starting = starting;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public boolean isActivationByAdmin() {
        return activationByAdmin;
    }

    public void setActivationByAdmin(boolean activationByAdmin) {
        this.activationByAdmin = activationByAdmin;
    }

    public AdminUserRest getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserRest createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

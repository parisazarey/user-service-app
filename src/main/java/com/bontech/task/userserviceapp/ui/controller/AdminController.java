package com.bontech.task.userserviceapp.ui.controller;


import com.bontech.task.userserviceapp.service.ActiveServiceService;
import com.bontech.task.userserviceapp.service.ServiceService;
import com.bontech.task.userserviceapp.service.SimpleUserService;
import com.bontech.task.userserviceapp.shared.dto.ActiveServiceDTO;
import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;
import com.bontech.task.userserviceapp.shared.dto.ServiceDTO;
import com.bontech.task.userserviceapp.shared.dto.SimpleUserDTO;
import com.bontech.task.userserviceapp.ui.model.request.ActiveServiceRequestModel;
import com.bontech.task.userserviceapp.ui.model.request.ServiceRequestModel;
import com.bontech.task.userserviceapp.ui.model.request.SimpleUserRequestModel;
import com.bontech.task.userserviceapp.ui.model.response.ActiveServiceRest;
import com.bontech.task.userserviceapp.ui.model.response.ServiceRest;
import com.bontech.task.userserviceapp.ui.model.response.SimpleUserRest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("admin/{adminUserId}")
public class AdminController {

    @Autowired
    private SimpleUserService simpleUserService;

    @Autowired
    private ServiceService serviceService;
    @Autowired
    private ActiveServiceService activeServiceService;


    @PostMapping(path = "/users", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public SimpleUserRest createSimpleUser(@PathVariable String adminUserId, @RequestBody SimpleUserRequestModel simpleUser) {

        ModelMapper modelMapper = new ModelMapper();
        SimpleUserDTO simpleUserDto = modelMapper.map(simpleUser, SimpleUserDTO.class);
        AdminUserDTO adminUserDTO = new AdminUserDTO();
        adminUserDTO.setUserId(adminUserId);
        simpleUserDto.setCreatedBy(adminUserDTO);
        SimpleUserDTO createdUser = simpleUserService.createSimpleUser(simpleUserDto);
        SimpleUserRest returnValue = modelMapper.map(createdUser, SimpleUserRest.class);
        return returnValue;
    }

    @GetMapping(path = "/users/{userId}", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public SimpleUserRest getUser(@PathVariable String userId) {
        ModelMapper modelMapper = new ModelMapper();
        SimpleUserDTO simpleUserDTO = simpleUserService.getSimpleUser(userId);
        SimpleUserRest returnValue = modelMapper.map(simpleUserDTO, SimpleUserRest.class);
        return returnValue;

    }

    @DeleteMapping(path = "/users/{userId}")
    public void deleteUser(@PathVariable String userId) {
        simpleUserService.deleteUser(userId);
    }

    @PutMapping(path = "users/{userId}", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public SimpleUserRest updateUser(@PathVariable String userId, @RequestBody SimpleUserRequestModel simpleUser) {
        ModelMapper modelMapper = new ModelMapper();
        SimpleUserDTO user = modelMapper.map(simpleUser, SimpleUserDTO.class);
        SimpleUserDTO updatedUser = simpleUserService.updateUser(userId, user);
        SimpleUserRest returnValue = modelMapper.map(updatedUser, SimpleUserRest.class);
        return returnValue;
    }


    @PostMapping(path = "/services", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ServiceRest createService(@PathVariable String adminUserId, @RequestBody ServiceRequestModel serviceRequest) {

        ModelMapper modelMapper = new ModelMapper();
        ServiceDTO serviceDTO = modelMapper.map(serviceRequest, ServiceDTO.class);
        AdminUserDTO adminUserDTO = new AdminUserDTO();
        adminUserDTO.setUserId(adminUserId);
        serviceDTO.setCreatedBy(adminUserDTO);
        ServiceDTO createdService = serviceService.createService(serviceDTO);
        ServiceRest returnValue = modelMapper.map(createdService, ServiceRest.class);
        return returnValue;
    }

    @GetMapping(path = "/services/{serviceId}", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ServiceRest getService(@PathVariable String serviceId) {
        ServiceDTO serviceDTO = serviceService.getService(serviceId);
        ServiceRest returnValue = new ModelMapper().map(serviceDTO, ServiceRest.class);
        return returnValue;

    }

    @DeleteMapping("/services/{serviceId}")
    public void deleteService(@PathVariable String serviceId) {
        serviceService.deleteService(serviceId);
    }

    @PutMapping(path = "/services/{serviceId}", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ServiceRest updateService(@PathVariable String serviceId, @RequestBody ServiceRequestModel serviceRequest) {
        ModelMapper modelMapper = new ModelMapper();
        ServiceDTO serviceDTO = modelMapper.map(serviceRequest, ServiceDTO.class);
        ServiceDTO updatedService = serviceService.updateService(serviceId, serviceDTO);
        ServiceRest returnValue = modelMapper.map(updatedService, ServiceRest.class);
        return returnValue;
    }

    @PostMapping(path = "/services/{serviceId}/actives", produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ActiveServiceRest createService(@PathVariable String adminUserId, @PathVariable String serviceId, @RequestBody ActiveServiceRequestModel activeServiceRequest) {

        ModelMapper modelMapper = new ModelMapper();
        ActiveServiceDTO activeServiceDTO = modelMapper.map(activeServiceRequest, ActiveServiceDTO.class);

        AdminUserDTO adminUserDTO = new AdminUserDTO();
        adminUserDTO.setUserId(adminUserId);
        activeServiceDTO.setCreatedBy(adminUserDTO);

        ServiceDTO serviceDTO = new ServiceDTO();
        serviceDTO.setServiceId(serviceId);
        activeServiceDTO.setService(serviceDTO);

        ActiveServiceDTO createdActiveService = activeServiceService.createActiveService(activeServiceDTO);
        ActiveServiceRest returnValue = modelMapper.map(createdActiveService, ActiveServiceRest.class);
        return returnValue;
    }


}

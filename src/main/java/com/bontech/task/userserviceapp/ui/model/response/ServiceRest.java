package com.bontech.task.userserviceapp.ui.model.response;

import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;

import java.util.Date;

public class ServiceRest {


    private String serviceId;
    private String name;
    private double price;
    private int maximumTimes;
    private AdminUserRest createdBy;
    private Date createdDate;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMaximumTimes() {
        return maximumTimes;
    }

    public void setMaximumTimes(int maximumTimes) {
        this.maximumTimes = maximumTimes;
    }

    public AdminUserRest getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserRest createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

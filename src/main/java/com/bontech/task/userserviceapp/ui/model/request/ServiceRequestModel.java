package com.bontech.task.userserviceapp.ui.model.request;

import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;

import java.util.Date;

public class ServiceRequestModel {

    private String name;
    private double price = -1;
    private int maximumTimes = -1;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMaximumTimes() {
        return maximumTimes;
    }

    public void setMaximumTimes(int maximumTimes) {
        this.maximumTimes = maximumTimes;
    }
}

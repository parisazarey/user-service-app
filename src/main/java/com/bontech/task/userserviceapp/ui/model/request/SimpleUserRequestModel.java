package com.bontech.task.userserviceapp.ui.model.request;


public class SimpleUserRequestModel {
    private String username;
    private String password;
    private double amount = -1;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}

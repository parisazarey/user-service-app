package com.bontech.task.userserviceapp;

import com.bontech.task.userserviceapp.service.AdminUserService;
import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class InitAdminUser {
    @Autowired
    private AdminUserService adminUserService;

    @Value("${admin.username}")
    private String username;

    @Value("${admin.password}")
    private String password;

    @PostConstruct
    public void init() {
        AdminUserDTO adminUser = new AdminUserDTO();
        adminUser.setUsername(username);
        adminUser.setPassword(password);
        adminUserService.createAdminUser(adminUser);
    }

}

package com.bontech.task.userserviceapp.security;

import org.springframework.beans.factory.annotation.Value;

public class SecurityConstants {
    public  final static String ADMIN_SIGNUP_URL = "/admin";
    public static final long EXPIRATION_TIME = 864000000;   //10day
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_SECRET = "djkfhlwejffnj4";
    public static final String ADMIN_LOGIN_URL = "/admin/login";

}

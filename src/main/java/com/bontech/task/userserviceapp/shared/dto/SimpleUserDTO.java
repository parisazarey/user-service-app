package com.bontech.task.userserviceapp.shared.dto;


import java.io.Serializable;
import java.util.Date;

public class SimpleUserDTO implements Serializable {
    private static final long serialVersionUID = -3259005897811943521L;


    private long id;
    private String userId;
    private String username;
    private String password;
    private String encryptedPassword;
    private double amount;
    private AdminUserDTO createdBy;
    private Date createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public AdminUserDTO getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserDTO createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

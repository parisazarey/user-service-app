package com.bontech.task.userserviceapp.shared.dto;


import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

public class ServiceDTO  implements Serializable {


    private static final long serialVersionUID = -2813219646399246966L;
    private String serviceId;
    private String name;
    private double price;
    private int maximumTimes;
    private AdminUserDTO createdBy;
    private Date createdDate;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMaximumTimes() {
        return maximumTimes;
    }

    public void setMaximumTimes(int maximumTimes) {
        this.maximumTimes = maximumTimes;
    }

    public AdminUserDTO getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserDTO createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

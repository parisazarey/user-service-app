package com.bontech.task.userserviceapp.shared.dto;

import java.io.Serializable;

public class AdminUserDTO implements Serializable {


    private static final long serialVersionUID = -4127420005953704347L;
    private long id;
    private String userId;
    private String username;
    private String password;
    private String encryptedPassword;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}

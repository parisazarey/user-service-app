package com.bontech.task.userserviceapp.shared.dto;

import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;
import com.bontech.task.userserviceapp.io.entity.ServiceEntity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

public class ActiveServiceDTO implements Serializable {




    private String activeServiceId;
    private ServiceDTO service;
    private Date starting;
    private Date ending;
    private boolean activationByAdmin = true;
    private AdminUserDTO createdBy;
    private Date createdDate;

    public String getActiveServiceId() {
        return activeServiceId;
    }

    public void setActiveServiceId(String activeServiceId) {
        this.activeServiceId = activeServiceId;
    }

    public ServiceDTO getService() {
        return service;
    }

    public void setService(ServiceDTO service) {
        this.service = service;
    }

    public Date getStarting() {
        return starting;
    }

    public void setStarting(Date starting) {
        this.starting = starting;
    }

    public Date getEnding() {
        return ending;
    }

    public void setEnding(Date ending) {
        this.ending = ending;
    }

    public boolean isActivationByAdmin() {
        return activationByAdmin;
    }

    public void setActivationByAdmin(boolean activationByAdmin) {
        this.activationByAdmin = activationByAdmin;
    }

    public AdminUserDTO getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(AdminUserDTO createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

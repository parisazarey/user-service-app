package com.bontech.task.userserviceapp.service;

import com.bontech.task.userserviceapp.shared.dto.ServiceDTO;

public interface ServiceService {
    ServiceDTO createService(ServiceDTO serviceDTO);

    ServiceDTO getService(String serviceId);

    void deleteService(String serviceId);

    ServiceDTO updateService(String serviceId, ServiceDTO serviceDTO);
}

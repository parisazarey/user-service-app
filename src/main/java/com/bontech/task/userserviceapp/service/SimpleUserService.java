package com.bontech.task.userserviceapp.service;


import com.bontech.task.userserviceapp.shared.dto.SimpleUserDTO;

public interface SimpleUserService {

    SimpleUserDTO createSimpleUser(SimpleUserDTO simpleUserDTO);

    SimpleUserDTO getSimpleUser(String userId);

    void deleteUser(String userId);

    SimpleUserDTO updateUser(String userId, SimpleUserDTO user);
}

package com.bontech.task.userserviceapp.service.impl;

import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;
import com.bontech.task.userserviceapp.io.repository.AdminUserRepository;
import com.bontech.task.userserviceapp.service.AdminUserService;
import com.bontech.task.userserviceapp.shared.Utils;
import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AdminUserServiceImpl implements AdminUserService {
    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private Utils utils;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public AdminUserDTO createAdminUser(AdminUserDTO adminUser) {
        ModelMapper modelMapper = new ModelMapper();
        if (adminUserRepository.findByUsername(adminUser.getUsername()) != null)
            throw new RuntimeException("username was taken");

        AdminUserEntity adminUserEntity = modelMapper.map(adminUser, AdminUserEntity.class);
        adminUserEntity.setUserId(utils.generateAdminUserId(30));
        adminUserEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(adminUser.getPassword()));

        AdminUserEntity storedUserDetails = adminUserRepository.save(adminUserEntity);
        AdminUserDTO returnValue = modelMapper.map(storedUserDetails, AdminUserDTO.class);
        return returnValue;
    }

    @Override
    public AdminUserDTO getUserByUsername(String username) {
        AdminUserEntity adminUserEntity = adminUserRepository.findByUsername(username);
        if (adminUserEntity == null) throw new UsernameNotFoundException(username);
        AdminUserDTO returnValue = new ModelMapper().map(adminUserEntity, AdminUserDTO.class);

        return returnValue;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AdminUserEntity adminUser = adminUserRepository.findByUsername(username);
        if (adminUser == null) throw new UsernameNotFoundException(username);
        return new User(adminUser.getUsername(), adminUser.getEncryptedPassword(), new ArrayList<>());

    }
}

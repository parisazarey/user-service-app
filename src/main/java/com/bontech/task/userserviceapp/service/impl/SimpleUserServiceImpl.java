package com.bontech.task.userserviceapp.service.impl;


import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;
import com.bontech.task.userserviceapp.io.entity.SimpleUserEntity;
import com.bontech.task.userserviceapp.io.repository.AdminUserRepository;
import com.bontech.task.userserviceapp.io.repository.SimpleUserRepository;
import com.bontech.task.userserviceapp.service.SimpleUserService;
import com.bontech.task.userserviceapp.shared.Utils;
import com.bontech.task.userserviceapp.shared.dto.SimpleUserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SimpleUserServiceImpl implements SimpleUserService {
    @Autowired
    private SimpleUserRepository simpleUserRepository;

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private Utils utils;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public SimpleUserDTO createSimpleUser(SimpleUserDTO simpleUser) {
        ModelMapper modelMapper = new ModelMapper();
        if (simpleUserRepository.findByUsername(simpleUser.getUsername()) != null)
            throw new RuntimeException("username was taken");

        SimpleUserEntity simpleUserEntity = modelMapper.map(simpleUser,SimpleUserEntity.class);

        AdminUserEntity createdBy = adminUserRepository.findByUserId(simpleUser.getCreatedBy().getUserId());
        if(createdBy == null) throw new RuntimeException("admin user is not valid");

        simpleUserEntity.setUserId(utils.generateSimpleUserId(30));
        simpleUserEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(simpleUser.getPassword()));
        simpleUserEntity.setCreatedDate(new Date());
        simpleUserEntity.setCreatedBy(createdBy);
        if(simpleUser.getAmount() == -1) simpleUserEntity.setAmount(0);

        SimpleUserEntity storedUserDetails = simpleUserRepository.save(simpleUserEntity);
        SimpleUserDTO returnValue = modelMapper.map(storedUserDetails, SimpleUserDTO.class);

        return returnValue;
    }

    @Override
    public SimpleUserDTO getSimpleUser(String userId) {

        SimpleUserEntity simpleUserEntity = simpleUserRepository.findByUserId(userId);
        if(simpleUserEntity == null) throw new UsernameNotFoundException(userId);

        SimpleUserDTO returnValue = new ModelMapper().map(simpleUserEntity , SimpleUserDTO.class);
        return returnValue;
    }

    @Override
    public void deleteUser(String userId) {
        SimpleUserEntity simpleUserEntity = simpleUserRepository.findByUserId(userId);
        if(simpleUserEntity == null) throw new UsernameNotFoundException(userId);

        simpleUserRepository.delete(simpleUserEntity);
    }

    @Override
    public SimpleUserDTO updateUser(String userId, SimpleUserDTO user) {

            SimpleUserEntity simpleUserEntity = simpleUserRepository.findByUserId(userId);
            if (simpleUserEntity == null)
                throw new UsernameNotFoundException(userId);
            if(user.getUsername()!= null) simpleUserEntity.setUsername(user.getUsername());
            if(user.getPassword()!= null) simpleUserEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            if(user.getAmount() != -1) simpleUserEntity.setAmount(user.getAmount());
            SimpleUserEntity updatedUserDetails = simpleUserRepository.save(simpleUserEntity);
            SimpleUserDTO returnValue = new ModelMapper().map(updatedUserDetails, SimpleUserDTO.class);
            return returnValue;
    }


}

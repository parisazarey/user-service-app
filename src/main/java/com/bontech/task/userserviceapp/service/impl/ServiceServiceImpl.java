package com.bontech.task.userserviceapp.service.impl;

import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;
import com.bontech.task.userserviceapp.io.entity.ServiceEntity;
import com.bontech.task.userserviceapp.io.entity.SimpleUserEntity;
import com.bontech.task.userserviceapp.io.repository.AdminUserRepository;
import com.bontech.task.userserviceapp.io.repository.ServiceRepository;
import com.bontech.task.userserviceapp.service.ServiceService;
import com.bontech.task.userserviceapp.shared.Utils;
import com.bontech.task.userserviceapp.shared.dto.ServiceDTO;
import com.bontech.task.userserviceapp.shared.dto.SimpleUserDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ServiceServiceImpl implements ServiceService {

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private Utils utils;


    @Override
    public ServiceDTO createService(ServiceDTO serviceDTO) {
        ModelMapper modelMapper = new ModelMapper();
        if (serviceRepository.findByName(serviceDTO.getName()) != null)
            throw new RuntimeException(" service name was taken");

        ServiceEntity serviceEntity = modelMapper.map(serviceDTO, ServiceEntity.class);

        AdminUserEntity createdBy = adminUserRepository.findByUserId(serviceDTO.getCreatedBy().getUserId());
        if (createdBy == null) throw new RuntimeException("admin user is not valid");

        serviceEntity.setServiceId(utils.generateServiceId(30));
        if(serviceDTO.getPrice() == -1) serviceEntity.setPrice(0);
        if(serviceDTO.getMaximumTimes()==-1) serviceEntity.setMaximumTimes(0);
        serviceEntity.setCreatedDate(new Date());
        serviceEntity.setCreatedBy(createdBy);

        ServiceEntity storedService = serviceRepository.save(serviceEntity);
        ServiceDTO  returnValue = modelMapper.map(storedService, ServiceDTO.class);

        return returnValue;
    }

    @Override
    public ServiceDTO getService(String serviceId) {

        ServiceEntity serviceEntity = serviceRepository.findByServiceId(serviceId);
        if(serviceEntity == null) throw new UsernameNotFoundException(serviceId);

       ServiceDTO returnValue = new ModelMapper().map(serviceEntity , ServiceDTO.class);
        return returnValue;
    }

    @Override
    public void deleteService(String serviceId) {
        ServiceEntity serviceEntity = serviceRepository.findByServiceId(serviceId);
        if(serviceEntity == null) throw new UsernameNotFoundException(serviceId);
        serviceRepository.delete(serviceEntity);
    }

    @Override
    public ServiceDTO updateService(String serviceId, ServiceDTO serviceDTO) {
        ServiceEntity serviceEntity = serviceRepository.findByServiceId(serviceId);
        if (serviceEntity == null) throw new UsernameNotFoundException(serviceId);

        if(serviceDTO.getName()!= null) serviceEntity.setName(serviceDTO.getName());
        if(serviceDTO.getPrice() != -1) serviceEntity.setPrice(serviceDTO.getPrice());
        if(serviceDTO.getMaximumTimes() != -1) serviceEntity.setMaximumTimes(serviceDTO.getMaximumTimes());

        ServiceEntity updatedService = serviceRepository.save(serviceEntity);
        ServiceDTO returnValue = new ModelMapper().map(updatedService, ServiceDTO.class);
        return returnValue;
    }
}

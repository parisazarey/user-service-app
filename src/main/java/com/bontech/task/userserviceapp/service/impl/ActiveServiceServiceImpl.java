package com.bontech.task.userserviceapp.service.impl;

import com.bontech.task.userserviceapp.io.entity.ActiveServiceEntity;
import com.bontech.task.userserviceapp.io.entity.AdminUserEntity;
import com.bontech.task.userserviceapp.io.entity.ServiceEntity;
import com.bontech.task.userserviceapp.io.repository.ActiveServiceRepository;
import com.bontech.task.userserviceapp.io.repository.AdminUserRepository;
import com.bontech.task.userserviceapp.io.repository.ServiceRepository;
import com.bontech.task.userserviceapp.service.ActiveServiceService;
import com.bontech.task.userserviceapp.shared.Utils;
import com.bontech.task.userserviceapp.shared.dto.ActiveServiceDTO;
import com.bontech.task.userserviceapp.shared.dto.ServiceDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ActiveServiceServiceImpl implements ActiveServiceService {
    @Autowired
    private ActiveServiceRepository activeServiceRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private Utils utils;


    @Override
    public ActiveServiceDTO createActiveService(ActiveServiceDTO activeServiceDTO) {
        ModelMapper modelMapper = new ModelMapper();


        if (!isDateValid(activeServiceDTO))
            throw new RuntimeException(" cant be created in this date and time");

        String serviceId = activeServiceDTO.getService().getServiceId();
        activeServiceDTO.setService(null);//for problem

        ActiveServiceEntity activeServiceEntity = modelMapper.map(activeServiceDTO, ActiveServiceEntity.class);

        AdminUserEntity createdBy = adminUserRepository.findByUserId(activeServiceDTO.getCreatedBy().getUserId());
        if (createdBy == null) throw new RuntimeException("admin user is not valid");

        ServiceEntity service = serviceRepository.findByServiceId(serviceId);
        if (service == null) throw new UsernameNotFoundException("service not exist");


        activeServiceEntity.setActiveServiceId(utils.generateActiveServiceId(30));
        activeServiceEntity.setCreatedDate(new Date());
        activeServiceEntity.setCreatedBy(createdBy);
        activeServiceEntity.setService(service);



        ActiveServiceEntity storedActiveService = activeServiceRepository.save(activeServiceEntity);
        ActiveServiceDTO returnValue = modelMapper.map(storedActiveService, ActiveServiceDTO.class);
        return returnValue;
    }

    private boolean isDateValid(ActiveServiceDTO activeServiceDTO) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(activeServiceDTO.getStarting());
        cal.add(Calendar.DAY_OF_YEAR, -1);
        Date oneDayBefore = cal.getTime();

        cal.setTime(activeServiceDTO.getStarting());
        cal.add(Calendar.DAY_OF_YEAR, 1);
        Date oneDayAfter = cal.getTime();

        ServiceEntity service = serviceRepository.findByServiceId(activeServiceDTO.getService().getServiceId());
        if (service == null) throw new UsernameNotFoundException("service not exist");

        List<ActiveServiceEntity> activeServices = activeServiceRepository.findAllByServiceAndStartingBetween(service, oneDayBefore, oneDayAfter);
        if (activeServices.isEmpty()) return true;

        return false;

    }
}

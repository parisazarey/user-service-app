package com.bontech.task.userserviceapp.service;

import com.bontech.task.userserviceapp.shared.dto.ActiveServiceDTO;

public interface ActiveServiceService {
    ActiveServiceDTO createActiveService(ActiveServiceDTO activeServiceDTO);
}

package com.bontech.task.userserviceapp.service;

import com.bontech.task.userserviceapp.shared.dto.AdminUserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AdminUserService  extends UserDetailsService {

    AdminUserDTO createAdminUser(AdminUserDTO adminUserDTO);

    AdminUserDTO getUserByUsername(String userName);
}
